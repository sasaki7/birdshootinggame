#import "GameCenter.h"
#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import <AppController.h>
#import "RootViewController.h"

@implementation GameCenter

+ (void) requestLoginGameCenter {
    if(floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_6_0) {
        GKLocalPlayer* player = [GKLocalPlayer localPlayer];
        UIViewController* rootController = [UIApplication sharedApplication].keyWindow.rootViewController;
        player.authenticateHandler = ^(UIViewController* ui, NSError* error) {
            if(nil != ui) {
                NSLog(@"Need to login");
                [rootController presentViewController:ui animated:YES completion:nil];
            } else if(player.isAuthenticated) {
                NSLog(@"Authenticated");
            } else {
                NSLog(@"Failed");
            }
        };
    } else {
        //6未満でのgamecenter認証コード
        GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
        
        [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
            if(localPlayer.authenticated) {
                NSLog(@"Authenticated");
            } else {
                NSLog(@"Failed authenticated");
            }
        }];
    }
}

+ (void) requestOpenRanking {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated)
    {
        AppController *appController = (AppController *)[UIApplication sharedApplication].delegate;
        
        GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc]init];
        leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
        leaderboardController.leaderboardDelegate = appController.viewController;
        
        [appController.viewController presentModalViewController:leaderboardController animated:YES];
    }
    else
    {
        NSString *alertTitle = @"";
        NSString *alertMessage = @"Game Centerへのログインが必要です。";
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

+ (void) requestPostScore :(NSNumber *)kind score:(NSNumber *)score {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if([localPlayer isAuthenticated])
    {
        NSString *idName = @"";
        
        switch ([kind intValue]) {
            case 1:
                idName = @"XXX";
                break;
            case 2:
                idName = @"YYY";
                break;
            default:
                break;
        }
        
        GKScore *gkScore = [[GKScore alloc]initWithCategory:idName];
        gkScore.value = [score longLongValue];
        gkScore.context = 1;
        [gkScore reportScoreWithCompletionHandler:^(NSError *error) {
            if(error)
            {
                NSLog(@"Error : %@",error);
            }
            else
            {
                NSLog(@"Sent highscore.");
            }
        }];
    }
    else
    {
        NSLog(@"Gamecenter not authenticated.");
    }
    
}

+ (void) requestOpenAchievement {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated)
    {
        AppController *appController = (AppController *)[UIApplication sharedApplication].delegate;
        
        GKAchievementViewController *achievementController = [[GKAchievementViewController alloc]init];
        achievementController.achievementDelegate = appController.viewController;
        
        [appController.viewController presentModalViewController:achievementController animated:YES];
        
    }
    else
    {
        NSString *alertTitle = @"";
        NSString *alertMessage = @"Game Centerへのログインが必要です。";
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

+ (void) requestPostAchievement :(NSNumber *)kind percentComplete:(NSNumber *)percentComplete {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if([localPlayer isAuthenticated])
    {
        NSString *idName = @"";
        
        switch ([kind intValue]) {
            case 1:
                idName = @"XXX";
                break;
            case 2:
                idName = @"YYY";
                break;
            default:
                break;
        }
        
        GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier:idName];
        achievement.percentComplete = [percentComplete doubleValue];
        achievement.showsCompletionBanner = YES;
        [achievement reportAchievementWithCompletionHandler:^(NSError *error) {
            if(error)
            {
                NSLog(@"Error : %@",error);
            }
            else
            {
                NSLog(@"Sent Achievement.");
            }
        }];
    }
    else
    {
        NSLog(@"Gamecenter not authenticated.");
    }
}

@end