#import "NativeBridge.h"
#import "GameCenter.h"

void NativeBridge::requestLoginGameCenter()
{
    [GameCenter requestLoginGameCenter];
}

void NativeBridge::requestOpenRanking()
{
    [GameCenter requestOpenRanking];
}

void NativeBridge::requestPostScore(int kind, int score)
{
    [GameCenter requestPostScore:[NSNumber numberWithInt:kind]
                           score:[NSNumber numberWithInt:score]];
}

void NativeBridge::requestOpenAchievement()
{
    [GameCenter requestOpenAchievement];
}

void NativeBridge::requestPostAchievement(int kind, int percentComplete)
{
    [GameCenter requestPostAchievement:[NSNumber numberWithInt:kind]
                       percentComplete:[NSNumber numberWithInt:percentComplete]];
}
