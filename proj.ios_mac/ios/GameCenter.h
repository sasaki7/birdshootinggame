@interface GameCenter : NSObject

+ (void) requestLoginGameCenter;
+ (void) requestOpenRanking;
+ (void) requestPostScore :(NSNumber *)kind score:(NSNumber *)score;
+ (void) requestOpenAchievement;
+ (void) requestPostAchievement :(NSNumber *)kind percentComplete:(NSNumber *)percentComplete;

@end
