package org.cocos2dx.cpp;

import android.os.Bundle;
import android.util.Log;

public class GooglePlayGameServices {

    private final static String TAG = GooglePlayGameServices.class.getSimpleName();

    public static void requestLoginGameCenter() {
        Log.d(TAG, "requestLoginGameCenter");
    }

    public static void requestOpenRanking() {

    }

    public static void requestPostScore(int kind, int score) {

    }

    public static void requestOpenAchievement() {

    }

    public static void requestPostAchievement(int kind, int percentComplete) {

    }
}