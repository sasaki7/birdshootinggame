#include <string>
#include "GooglePlayGameServicesJni.h"
#include <jni.h>
#include "jni/JniHelper.h"

using namespace cocos2d;

namespace {
    static const std::string packageName = "org/cocos2dx/cpp/GooglePlayGameServices";
}

namespace GooglePlayGameServicesJni {

    void requestLoginGameCenter() {
        JniMethodInfo jmInfo;
        if (JniHelper::getStaticMethodInfo(jmInfo, packageName.c_str(), "requestLoginGameCenter", "()V")) {
            jmInfo.env->CallStaticVoidMethod(jmInfo.classID, jmInfo.methodID);
            jmInfo.env->DeleteLocalRef(jmInfo.classID);
        }
    }

    void requestOpenRanking() {
        JniMethodInfo jmInfo;
        if (JniHelper::getStaticMethodInfo(jmInfo, packageName.c_str(), "requestOpenRanking", "()V")) {
            jmInfo.env->CallStaticVoidMethod(jmInfo.classID, jmInfo.methodID);
            jmInfo.env->DeleteLocalRef(jmInfo.classID);
        }
    }

    void requestPostScore(int kind, int score) {
        JniMethodInfo jmInfo;
        if (JniHelper::getStaticMethodInfo(jmInfo, packageName.c_str(), "requestPostScore", "(II)V")) {
            jmInfo.env->CallStaticVoidMethod(jmInfo.classID, jmInfo.methodID, kind, score);
            jmInfo.env->DeleteLocalRef(jmInfo.classID);
        }
    }

    void requestOpenAchievement() {
        JniMethodInfo jmInfo;
        if (JniHelper::getStaticMethodInfo(jmInfo, packageName.c_str(), "requestOpenAchievement", "()V")) {
            jmInfo.env->CallStaticVoidMethod(jmInfo.classID, jmInfo.methodID);
            jmInfo.env->DeleteLocalRef(jmInfo.classID);
        }
    }

    void requestPostAchievement(int kind, int percentComplete) {
        JniMethodInfo jmInfo;
        if (JniHelper::getStaticMethodInfo(jmInfo, packageName.c_str(), "requestPostAchievement", "(II)V")) {
            jmInfo.env->CallStaticVoidMethod(jmInfo.classID, jmInfo.methodID, kind, percentComplete);
            jmInfo.env->DeleteLocalRef(jmInfo.classID);
        }
    }

/*    void testJni(const char* str){
        JniMethodInfo t;
        if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "method1", "(Ljava/lang/String;)V")) {
            jstring arg1 = t.env->NewStringUTF(str);
            t.env->CallStaticVoidMethod(t.classID, t.methodID, arg1);
            t.env->DeleteLocalRef(arg1);
        }
    }

    JNIEXPORT void JNICALL Java_org_cocos2dx_lib_NativeTest_onMethod1(JNIEnv *env, jobject thiz, jstring str) {
        if(_delegate) {
            const char *arg1 = env->GetStringUTFChars(str, NULL);

            native_plugin::NativeDelegate *nativeDelegate = (native_plugin::NativeDelegate*)_delegate;
            nativeDelegate->method1(arg1);
        }
    }
*/
}
