#include "NativeBridge.h"
#include "GooglePlayGameServicesJni.h"

void NativeBridge::requestLoginGameCenter()
{
    GooglePlayGameServicesJni::requestLoginGameCenter();
}

void NativeBridge::requestOpenRanking()
{
    GooglePlayGameServicesJni::requestOpenRanking();
}

void NativeBridge::requestPostScore(int kind, int score)
{
    GooglePlayGameServicesJni::requestPostScore(kind, score);
}

void NativeBridge::requestOpenAchievement()
{
    GooglePlayGameServicesJni::requestOpenAchievement();
}

void NativeBridge::requestPostAchievement(int kind, int percentComplete)
{
    GooglePlayGameServicesJni::requestPostAchievement(kind, percentComplete);
}
