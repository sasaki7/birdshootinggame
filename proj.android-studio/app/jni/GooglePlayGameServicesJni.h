#pragma once

namespace GooglePlayGameServicesJni
{
    void requestLoginGameCenter();
    void requestOpenRanking();
    void requestPostScore(int kind, int score);
    void requestOpenAchievement();
    void requestPostAchievement(int kind, int percentComplete);
}
