#pragma once

#include "BulletData.h"

class Bullet
{
public:
    explicit Bullet(std::shared_ptr<BulletData> bulletData, cocos2d::Node* bulletView);
    ~Bullet();
    
    void updateMove();
private:
    
    CC_SYNTHESIZE(std::shared_ptr<BulletData>, _bulletData, BulletData);
    CC_SYNTHESIZE(cocos2d::Node*, _bulletView, BulletView);
};
