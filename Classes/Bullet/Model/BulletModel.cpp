#include "BulletModel.h"
#include "LSGController.h"
#include "LiziParticleView.h"

USING_NS_CC;

BulletModel::BulletModel()
: _bullets()
{
}

BulletModel::~BulletModel()
{
}

void BulletModel::update(float deltaTime)
{
    for (auto it = std::begin(_bullets) ; it != std::end(_bullets) ;) {
        if ((*it)->getBulletData()->getIsAlive()) {
            (*it)->updateMove();
            ++it;
        } else {
            it = _bullets.erase(it);
        }
    }
}

void BulletModel::firingBullet(Node* attachNode,
                               const std::shared_ptr<UnitData> owner,
                               const Vec2& spawnPoint,
                               float directionAngle)
{
    auto bulletView = LiziParticleView::create();
    bulletView->setScale(0.6f);
    bulletView->setPosition(spawnPoint);
    attachNode->addChild(bulletView);
    
    auto bulletData = std::make_shared<BulletData>(owner, directionAngle);
    _bullets.push_back(std::make_shared<Bullet>(bulletData, bulletView));
}
