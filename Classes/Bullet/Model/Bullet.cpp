#include "Bullet.h"
#include "Common.h"

USING_NS_CC;
using namespace Common;

Bullet::Bullet(std::shared_ptr<BulletData> bulletData, Node* bulletView)
: _bulletData(bulletData)
, _bulletView(bulletView)
{
}

Bullet::~Bullet()
{
    _bulletView->removeFromParent();
}

void Bullet::updateMove()
{
    Vec2 velocity = {
        _bulletData->getSpeed() * std::cos(degreeToRadian(_bulletData->getDirectionAngle())),
        _bulletData->getSpeed() * std::sin(degreeToRadian(_bulletData->getDirectionAngle()))
    };
    _bulletView->setPosition(_bulletView->getPosition() + velocity);
    
    if (isOutOfScreen(_bulletView))
        getBulletData()->setIsAlive(false);
}
