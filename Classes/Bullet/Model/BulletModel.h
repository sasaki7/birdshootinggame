#pragma once

#include "LSGModel.hpp"
#include "Bullet.h"
#include "UnitData.h"

class LSGController;

class BulletModel : public LSGModel
{
public:
    using Bullets = std::vector<std::shared_ptr<Bullet>>;
    
    BulletModel();
    virtual ~BulletModel();
    
    void update(float deltaTime);
    
    void firingBullet(cocos2d::Node* attachNode,
                      const std::shared_ptr<UnitData> owner,
                      const cocos2d::Vec2& spawnPoint,
                      float directionAngle);

private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(Bullets, _bullets, Bullets);
};
