#pragma once

#include "UnitData.h"

class BulletData
{
public:
    enum class Orbit {
        Straight,
    };
    static constexpr float defaultBulletSpeed = 10.5f;
    
    explicit BulletData(const std::shared_ptr<UnitData> gunner, float directionAngle)
    : _gunner(gunner)
    , _directionAngle(directionAngle)
    , _isAlive(true)
    , _speed(defaultBulletSpeed)
    , _orbit(Orbit::Straight)
    {}
    
    virtual ~BulletData() = default;
    
    CC_SYNTHESIZE(std::shared_ptr<UnitData>, _gunner, Gunner);
    CC_SYNTHESIZE(float, _directionAngle, DirectionAngle);
    CC_SYNTHESIZE(bool, _isAlive, IsAlive);
    CC_SYNTHESIZE(float, _speed, Speed);
    CC_SYNTHESIZE(Orbit, _orbit, Orbit);
};
