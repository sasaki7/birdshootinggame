#pragma once

#include "cocos2d.h"

class NativeBridge
{
    CC_DISALLOW_COPY_AND_ASSIGN(NativeBridge)
public:
    static void requestLoginGameCenter();
    static void requestOpenRanking();
    static void requestPostScore(int kind, int score);
    static void requestOpenAchievement();
    static void requestPostAchievement(int kind, int percentComplete);
};
