#pragma once

#include <string>
#include "Macros.h"

namespace ResourceFinder {
    
    enum class ImageType0 {
        // ------------------------------------
        // Title UI
        TitleLogo,
        TitleHeader,
        // ------------------------------------
        
        // ------------------------------------
        // Game UI
        GetReady,
        GameOver,
        GameBackground,
        HitEffect,
        ControllerShotButton,
        ControllerUpButton,
        ControllerDownButton,
        ControllerLeftButton,
        ControllerRightButton,
        // ------------------------------------
        
        // ------------------------------------
        // Result UI
        WindowScore,
        // ------------------------------------
        
        // ------------------------------------
        // Common UI
        PlayButton,
        BackButton,
        RankingButton,
        // ------------------------------------
    };
    
    enum class ImageType1 {
        // ------------------------------------
        // Bullet
        Bullet,
        // ------------------------------------
    };
    
    enum class PlayerImageType {
        // ------------------------------------
        // Player State Image
        Idle1,
        Idle2,
        Idle3,
        Idle4,
        Fly1,
        Fly2,
        Fly3,
        Hurt,
        Ko
        // ------------------------------------
    };
    
    enum class EnemyImageType {
        // ------------------------------------
        // Enemy
        Enemy1,
        Enemy2,
        Enemy3,
        Enemy4,
        Boss
        // ------------------------------------
    };
    
    const std::string pathForImage(ImageType0 imageType);
    const std::string pathForImage(ImageType1 imageType, int index);
    const std::string pathForImage(PlayerImageType imageType);
    const std::string pathForImage(EnemyImageType imageType);
    
} // namespace ResourceFinder

// for unordered_map
ENUM_HASH(ResourceFinder::ImageType0)
ENUM_HASH(ResourceFinder::ImageType1)
ENUM_HASH(ResourceFinder::PlayerImageType)
ENUM_HASH(ResourceFinder::EnemyImageType)
