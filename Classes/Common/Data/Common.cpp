#include "Common.h"
#include "cocos2d.h"

USING_NS_CC;

namespace Common {
    bool isOutOfScreen(Node* node) {
        return node->getPosition().x + node->getContentSize().width < 0.f ||
               node->getPosition().x > Director::getInstance()->getWinSize().width;
    }
    
//    bool isCollisionCircleVsPoint(float radius, Vec2& centerPoint, Vec2& point) {
//        return radius >= centerPoint.distance(point);
//    }
} // namespace Common