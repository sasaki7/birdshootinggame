#pragma once

namespace cocos2d {
    class Node;
}

namespace Common {
    static constexpr float PI = 3.141592653589793f;
    
    template<class T> T radianToDegree(T radian);
    template<class T> T degreeToRadian(T degree);
    
    bool isOutOfScreen(cocos2d::Node* node);
//    bool isCollisionCircleVsPoint(float radius, cocos2d::Vec2& centerPoint, cocos2d::Vec2& point);
    
} // namespace Common

template<class T>
T Common::radianToDegree(T radian) {
    static const T multiplicationValue = 180.f / PI;
    return radian * multiplicationValue;
}

template<class T>
T Common::degreeToRadian(T degree) {
    static const T multiplicationValue = PI / 180.f;
    return degree * multiplicationValue;
}
