#include <unordered_map>
#include "ResourceFinder.h"
#include "cocos2d.h"

USING_NS_CC;

namespace ResourceFinder {
    
    const std::string pathForImage(ImageType0 imageType) {
        static const std::unordered_map<ImageType0, std::string> imageTypeToPaths = {
            // ------------------------------------
            // Title UI
            {ImageType0::TitleLogo, "gui/lotus_logo.png"},
            {ImageType0::TitleHeader, "gui/title.png"},
            // ------------------------------------
            
            // ------------------------------------
            // Game UI
            {ImageType0::GetReady, "gui/text_getReady.png"},
            {ImageType0::GameOver, "gui/text_gameOver.png"},
            {ImageType0::GameBackground, "background/background.png"},
            {ImageType0::HitEffect, "gui/hitEffect.png"},
            {ImageType0::ControllerShotButton, "gui/weapon_a.png"},
            {ImageType0::ControllerUpButton, "gui/top.png"},
            {ImageType0::ControllerDownButton, "gui/under.png"},
            {ImageType0::ControllerLeftButton, "gui/left.png"},
            {ImageType0::ControllerRightButton, "gui/right.png"},
            // ------------------------------------
            
            // ------------------------------------
            // Result UI
            {ImageType0::WindowScore, "gui/window_score.png"},
            // ------------------------------------
            
            // ------------------------------------
            // Common UI
            {ImageType0::PlayButton, "gui/button_play.png"},
            {ImageType0::BackButton, "gui/button_back.png"},
            {ImageType0::RankingButton, "gui/button_rank.png"},
            // ------------------------------------
        };
        
        auto it = imageTypeToPaths.find(imageType);
        return it != std::end(imageTypeToPaths)? it->second : "";
    }
    
    const std::string pathForImage(ImageType1 imageType, int index) {
        static const std::unordered_map<ImageType1, std::string> imageTypeToPaths = {
            // ------------------------------------
            // Bullet
            {ImageType1::Bullet, "bullet/bullet_%02d.png"}
            // ------------------------------------
        };
        auto it = imageTypeToPaths.find(imageType);
        return it != std::end(imageTypeToPaths)? StringUtils::format(it->second.c_str(), index) : "";
    }
    
    const std::string pathForImage(PlayerImageType imageType) {
        static const std::unordered_map<PlayerImageType, std::string> imageTypeToPaths = {
            {PlayerImageType::Idle1, "player/player_idle_f01.png"},
            {PlayerImageType::Idle2, "player/player_idle_f02.png"},
            {PlayerImageType::Idle3, "player/player_idle_f03.png"},
            {PlayerImageType::Idle4, "player/player_idle_f04.png"},
            {PlayerImageType::Fly1,  "player/player_fly_f01.png"},
            {PlayerImageType::Fly2,  "player/player_fly_f02.png"},
            {PlayerImageType::Fly3,  "player/player_fly_f03.png"},
            {PlayerImageType::Hurt,  "player/player_hurt_f01.png"},
            {PlayerImageType::Ko,    "player/player_ko_f01.png"}
        };
        
        auto it = imageTypeToPaths.find(imageType);
        return it != std::end(imageTypeToPaths)? it->second : "";
    }
    
    const std::string pathForImage(EnemyImageType imageType) {
        static const std::unordered_map<EnemyImageType, std::string> imageTypeToPaths = {
            {EnemyImageType::Enemy1, "enemy/enemy01.png"},
            {EnemyImageType::Enemy2, "enemy/enemy02.png"},
            {EnemyImageType::Enemy3, "enemy/enemy03.png"},
            {EnemyImageType::Enemy4, "enemy/enemy04.png"},
            {EnemyImageType::Boss, ""}  // @todo: ボス画像
        };
        auto it = imageTypeToPaths.find(imageType);
        return it != std::end(imageTypeToPaths)? it->second : "";
    }
    
} // namespace ResourceFinder
