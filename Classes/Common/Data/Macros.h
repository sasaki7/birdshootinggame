#pragma once

// for std::unordered_map
#define ENUM_HASH(class)\
namespace std {\
    template<>\
    struct hash<class> {\
        std::size_t operator()(class key) const {\
            typedef std::underlying_type<class>::type enumUnderlyingType;\
            return static_cast<enumUnderlyingType>(key);\
        }\
    };\
}

#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
    #define LSG_DEPRECATED_ATTRIBUTE_MSG(msg) __attribute__((deprecated(msg)))
    #define LSG_DEPRECATED_ATTRIBUTE CC_DEPRECATED_ATTRIBUTE
#elif _MSC_VER >= 1400
    #define LSG_DEPRECATED_ATTRIBUTE_MSG(msg) __declspec(deprecated(msg))
    #define LSG_DEPRECATED_ATTRIBUTE CC_DEPRECATED_ATTRIBUTE
#else
    #define LSG_DEPRECATED_ATTRIBUTE_MSG(msg) CC_DEPRECATED_ATTRIBUTE
    #define LSG_DEPRECATED_ATTRIBUTE CC_DEPRECATED_ATTRIBUTE
#endif
