#pragma once

#include "cocos2d.h"
#include "ResourceFinder.h"

class LiziParticleView : public cocos2d::ParticleSystemQuad
{
public:
    CREATE_FUNC(LiziParticleView);
    
protected:
    LiziParticleView() {}
    virtual ~LiziParticleView() {}
    
    bool init() override;
};
