#pragma once

#include "LSGController.h"
#include "GameModel.h"

class GameController : public LSGController
{
public:
    CREATE_CONTROLLER_FUNC(GameController);
    
protected:
    GameController();
    virtual ~GameController();
    
    bool init() override;
    
    void update(float deltaTime) override;

    void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
    void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
    void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
    void onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
    
private:
    std::unique_ptr<GameModel> _gameModel;
};
