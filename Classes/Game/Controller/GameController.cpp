#include "GameController.h"
#include "PlayerView.h"
#include "ResultController.h"
#include "LSGNavigator.h"
#include "LSGLog.hpp"

USING_NS_CC;

GameController::GameController()
: _gameModel()
{
}

GameController::~GameController()
{
}

bool GameController::init()
{
    if (!Layer::init())
        return false;
    
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan = std::bind(&GameController::onTouchesBegan, this, std::placeholders::_1, std::placeholders::_2);
    listener->onTouchesMoved = std::bind(&GameController::onTouchesMoved, this, std::placeholders::_1, std::placeholders::_2);
    listener->onTouchesEnded = std::bind(&GameController::onTouchesEnded, this, std::placeholders::_1, std::placeholders::_2);
    listener->onTouchesCancelled = std::bind(&GameController::onTouchesCancelled, this, std::placeholders::_1, std::placeholders::_2);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    LSGNavigator::Singleton::getInstance().addCustomEvent("onGameFinish", [this](EventCustom* event) {
        auto resultScore = static_cast<Value*>(event->getUserData());
        LSGNavigator::Singleton::getInstance().runScene(ResultController::create(resultScore->asInt()));
    });
    
    _gameModel.reset(new GameModel());
    _gameModel->init(this);
    scheduleUpdate();
    return true;
}

void GameController::update(float deltaTime)
{
    _gameModel->update(deltaTime);
}

void GameController::onTouchesBegan(const std::vector<Touch*>& touches, Event* event)
{
    _gameModel->onTouchesBegan(touches, event);
}

void GameController::onTouchesMoved(const std::vector<Touch*>& touches, Event* event)
{
    _gameModel->onTouchesMoved(touches, event);
}

void GameController::onTouchesEnded(const std::vector<Touch*>& touches, Event* event)
{
    _gameModel->onTouchesEnded(touches, event);
}

void GameController::onTouchesCancelled(const std::vector<Touch*>& touches, Event* event)
{
    _gameModel->onTouchesCancelled(touches, event);
}
