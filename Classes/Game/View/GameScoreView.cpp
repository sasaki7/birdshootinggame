#include "GameScoreView.h"

USING_NS_CC;

void GameScoreView::updateScore(int score)
{
    _scoreLabel->setString(StringUtils::format("Score : %05d", score));
}

bool GameScoreView::init()
{
    if (!Node::init())
        return false;
    _scoreLabel = Label::create();
    _scoreLabel->setSystemFontSize(40.f);
    _scoreLabel->setColor(Color3B::BLACK);
    _scoreLabel->setString(StringUtils::format("Score : %05d", 0));
    addChild(_scoreLabel);
    return true;
}
