#include "GameTimerView.h"

USING_NS_CC;

void GameTimerView::updateTime(float time)
{
    _timerLabel->setString(StringUtils::format("Time : %04.2f", time));
}

bool GameTimerView::init()
{
    if (!Node::init())
        return false;
    _timerLabel = Label::create();
    _timerLabel->setSystemFontSize(40.f);
    _timerLabel->setColor(Color3B::BLACK);
    addChild(_timerLabel);
    return true;
}
