#include <unordered_map>
#include "GameControllerView.h"
#include "ResourceFinder.h"

USING_NS_CC;
using namespace ResourceFinder;

namespace {
    static const std::unordered_map<ControllerType, std::string> controllerTypeToImagePaths = {
        {ControllerType::Up, pathForImage(ImageType0::ControllerUpButton)},
        {ControllerType::Down, pathForImage(ImageType0::ControllerDownButton)},
        {ControllerType::Left, pathForImage(ImageType0::ControllerLeftButton)},
        {ControllerType::Right, pathForImage(ImageType0::ControllerRightButton)},
        {ControllerType::Shot, pathForImage(ImageType0::ControllerShotButton)}
    };
} // namespace

GameControllerView* GameControllerView::create(ControllerType controllerType)
{
    auto p = new GameControllerView();
    if (p && p->init(controllerType)) {
        p->autorelease();
    } else {
        CC_SAFE_DELETE(p);
    }
    return p;
}

bool GameControllerView::init(ControllerType controllerType)
{
    _controllerType = controllerType;
    return Sprite::initWithFile(controllerTypeToImagePaths.at(_controllerType));
}

