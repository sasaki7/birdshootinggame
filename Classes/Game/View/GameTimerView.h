#pragma once

#include "cocos2d.h"

class GameTimerView : public cocos2d::Node
{
public:
    CREATE_FUNC(GameTimerView);
    
    void updateTime(float time);
protected:
    GameTimerView() {}
    virtual ~GameTimerView() {}
    
    bool init() override;
    
private:
    cocos2d::Label* _timerLabel;
};
