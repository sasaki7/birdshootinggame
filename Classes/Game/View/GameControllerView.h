#pragma once

#include "cocos2d.h"
#include "Macros.h"

enum class ControllerType {
    Up,
    Down,
    Left,
    Right,
    Shot
};

class GameControllerView : public cocos2d::Sprite
{
public:
    static GameControllerView* create(ControllerType controllerType);
    
protected:
    GameControllerView() {}
    virtual ~GameControllerView() {}
    
    bool init(ControllerType controllerType);
    
private:
    CC_SYNTHESIZE(ControllerType, _controllerType, ControllerType);
};

ENUM_HASH(ControllerType);
