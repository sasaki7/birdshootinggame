#pragma once

#include "cocos2d.h"

class GameScoreView : public cocos2d::Node
{
public:
    CREATE_FUNC(GameScoreView);
    
    void updateScore(int score);
protected:
    GameScoreView() {}
    virtual ~GameScoreView() {}
    
    bool init();
    
private:
    cocos2d::Label* _scoreLabel;
};
