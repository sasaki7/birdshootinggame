#pragma once

#include "cocos2d.h"

class GameBackGroundView : public cocos2d::Node
{
public:
    CREATE_FUNC(GameBackGroundView);
    
    void updateScroll();
protected:
    GameBackGroundView() {}
    virtual ~GameBackGroundView() {}
    
    bool init() override;
    
private:
    std::vector<cocos2d::Node*> _sprites;
};
