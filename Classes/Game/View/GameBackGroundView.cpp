#include "GameBackGroundView.h"
#include "ResourceFinder.h"

USING_NS_CC;
using namespace ResourceFinder;

void GameBackGroundView::updateScroll()
{
    for (auto bgSprite : _sprites) {
        auto scrollSpeed = bgSprite->getContentSize().width / 150.f;
        auto position = bgSprite->getPosition();
        position.x -= scrollSpeed;
        if (position.x <= -bgSprite->getContentSize().width) {
            position.x = bgSprite->getContentSize().width - (scrollSpeed + 8.f);
        }
        bgSprite->setPosition(position);
    }
}

bool GameBackGroundView::init()
{
    if (!Node::init())
        return false;
    auto numberOfBackground = 2;
    for (int i = 0 ; i < numberOfBackground ; i++) {
        auto bgSprite = Sprite::create(pathForImage(ImageType0::GameBackground));
        bgSprite->setPosition(bgSprite->getContentSize().width * i,
                              Director::getInstance()->getWinSize().height / 2);
        _sprites.push_back(bgSprite);
        addChild(bgSprite);
    }
    return true;
}
