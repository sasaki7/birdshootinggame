#include <random>
#include "GameModel.h"
#include "GameController.h"
#include "ResourceFinder.h"
#include "value.h"
#include "EnemyView.h"
#include "GameBackGroundView.h"
#include "GameTimerView.h"
#include "GameScoreView.h"
#include "GameControllerView.h"
#include "LiziParticleView.h"
#include "LSGNavigator.h"
#include "LSGLog.hpp"

USING_NS_CC;
using namespace ResourceFinder;

namespace {
    static const float defaultGameSeconds = 60.f;
    static const int enemyCrushingScore = 100;
    static const float playerBulletAngle = 0.f;
} // namespace

GameModel::GameModel()
: _gameState(GameState::Enter)
, _gameUpdates()
, _timer(defaultGameSeconds)
, _score(0)
, _uiViews(nullptr)
, _playerModel()
, _enemyModel()
, _bulletModel()
, _moveControllDatas()
, _particleBatchNode(nullptr)
{
}

GameModel::~GameModel()
{
}

bool GameModel::init(LSGController* controller)
{
    if (!LSGModel::init(controller))
        return false;

    const auto& director = Director::getInstance();
    _uiViews.reset(new GameUIViews());
    _uiViews->rootNode = Node::create();
    _uiViews->rootNode->setContentSize(director->getWinSize());
    controller->addChild(_uiViews->rootNode);
    
    // add background view
    _uiViews->backGroundView = GameBackGroundView::create();
    _uiViews->rootNode->addChild(_uiViews->backGroundView);
    
    // add timer view
    _uiViews->timerView = GameTimerView::create();
    _uiViews->timerView->setPosition(director->getWinSize().width - 200.f, director->getWinSize().height / 2 + 250.f);
    _uiViews->rootNode->addChild(_uiViews->timerView);
    
    // add score view
    _uiViews->scoreView = GameScoreView::create();
    _uiViews->scoreView->setPosition(200.f, director->getWinSize().height / 2 + 250.f);
    _uiViews->rootNode->addChild(_uiViews->scoreView);
    
    // add controller views
    auto controllerUpView = GameControllerView::create(ControllerType::Up);
    controllerUpView->setPosition(controllerUpView->getContentSize().width * 4 / 2,
                                  controllerUpView->getContentSize().height * 2 + 30.f);
    _uiViews->controllerViews[ControllerType::Up] = controllerUpView;
    _uiViews->rootNode->addChild(controllerUpView);
    
    auto controllerDownView = GameControllerView::create(ControllerType::Down);
    controllerDownView->setPosition(controllerDownView->getContentSize().width * 4 / 2,
                                    controllerDownView->getContentSize().height);
    _uiViews->controllerViews[ControllerType::Down] = controllerDownView;
    _uiViews->rootNode->addChild(controllerDownView);
    
    auto controllerLeftView = GameControllerView::create(ControllerType::Left);
    controllerLeftView->setPosition(controllerLeftView->getContentSize().width + controllerLeftView->getContentSize().width / 8.f,
                                    controllerLeftView->getContentSize().height * 1.5f);
    _uiViews->controllerViews[ControllerType::Left] = controllerLeftView;
    _uiViews->rootNode->addChild(controllerLeftView);
    
    auto controllerRightView = GameControllerView::create(ControllerType::Right);
    controllerRightView->setPosition(controllerRightView->getContentSize().width * 3 + controllerLeftView->getContentSize().width / 8.f,
                                     controllerRightView->getContentSize().height * 1.5f);
    _uiViews->controllerViews[ControllerType::Right] = controllerRightView;
    _uiViews->rootNode->addChild(controllerRightView);
    
    auto controllerShotView = GameControllerView::create(ControllerType::Shot);
    controllerShotView->setPosition(director->getWinSize().width - controllerShotView->getContentSize().width * 1.7f,
                                    controllerShotView->getContentSize().height * 2 - 25.f);
    _uiViews->controllerViews[ControllerType::Shot] = controllerShotView;
    _uiViews->rootNode->addChild(controllerShotView);
    
    _playerModel.reset(new PlayerModel());
    _playerModel->init(controller);
    
    _enemyModel.reset(new EnemyModel());
    _enemyModel->init(controller);
    
    _bulletModel.reset(new BulletModel());
    _bulletModel->init(controller);
    
    auto particleView = LiziParticleView::create();
    _particleBatchNode = ParticleBatchNode::createWithTexture(particleView->getTexture());
    controller->addChild(_particleBatchNode);
    
    _gameUpdates[GameState::Enter] = std::bind(&GameModel::updateGameEnterState, this, std::placeholders::_1);
    _gameUpdates[GameState::Exec] = std::bind(&GameModel::updateGameExecState, this, std::placeholders::_1);
    _gameUpdates[GameState::Exit] = std::bind(&GameModel::updateGameExitState, this, std::placeholders::_1);
    return true;
}

void GameModel::update(float deltaTime)
{
    _gameUpdates[_gameState](deltaTime);
}

void GameModel::onTouchesBegan(const std::vector<Touch*>& touches, Event* event)
{
    if (isRejectTouch())
        return;
    for (const auto& touch : touches) {        
        updateMoveControllerByTouchBegan(touch);
        updateShotControllerByTouch(touch);
    }
}

void GameModel::onTouchesMoved(const std::vector<Touch*>& touches, Event* event)
{
    if (isRejectTouch())
        return;
    for (const auto& touch : touches) {
        updateMoveControllerByTouchMoved(touch);
    }
}

void GameModel::onTouchesEnded(const std::vector<Touch*>& touches, Event* event)
{
    if (isRejectTouch())
        return;
    for (const auto& touch : touches) {
        removeMoveController(touch);
    }
}

void GameModel::onTouchesCancelled(const std::vector<Touch*>& touches, Event* event)
{
    this->onTouchesEnded(touches, event);
}

void GameModel::updateGameEnterState(float deltaTime)
{
    _playerModel->update(deltaTime);
    
    if (!_uiViews->getReadyEffectNode) {
        const auto& winSize = Director::getInstance()->getWinSize();
        auto getReadyEffectNode = Sprite::create(pathForImage(ResourceFinder::ImageType0::GetReady));
        getReadyEffectNode->setPosition(-getReadyEffectNode->getContentSize().width, winSize.height / 2);
        _uiViews->rootNode->addChild(getReadyEffectNode);
        auto inMove = MoveTo::create(0.8f, {winSize.width / 2, winSize.height / 2});
        auto delay = DelayTime::create(1.2f);
        auto outMove = MoveTo::create(0.8f, {winSize.width + getReadyEffectNode->getContentSize().width, winSize.height / 2});
        getReadyEffectNode->runAction(Sequence::create(inMove,
                                                       delay,
                                                       outMove,
                                                       CallFunc::create([this]() {
                                                            _gameState = GameState::Exec;
                                                       }),
                                                       RemoveSelf::create(),
                                                       nullptr));
        _uiViews->getReadyEffectNode = getReadyEffectNode;
    }
    
}

void GameModel::updateGameExecState(float deltaTime)
{
    _timer = std::max({_timer - deltaTime, 0.f});
    if (_timer <= 0.f) {
        _gameState = GameState::Exit;
    }
    
    _playerModel->update(deltaTime);
    
    for (const auto& moveControll : _moveControllDatas)
        _playerModel->moveByController(moveControll->controllerType);
    
    _enemyModel->update(deltaTime);
    
    _bulletModel->update(deltaTime);
    
    // player bullets vs bullets collision
    for (auto bullet : _bulletModel->getBullets())
        collisionForPlayerBulletVsEnemys(bullet);
    
    // update ui
    updateGameUI(deltaTime);
    
    updateLotterySpawnEnemy();
}

void GameModel::updateGameExitState(float deltaTime)
{
    _playerModel->update(deltaTime);
    
    _enemyModel->update(deltaTime);
    
    _bulletModel->update(deltaTime);
    
    if (!_uiViews->gameOverEffectNode) {
        const auto& winSize = Director::getInstance()->getWinSize();
        auto gameOverEffectNode = Sprite::create(pathForImage(ResourceFinder::ImageType0::GameOver));
        gameOverEffectNode->setPosition(-gameOverEffectNode->getContentSize().width, winSize.height / 2);
        _uiViews->rootNode->addChild(gameOverEffectNode);
        auto inMove = MoveTo::create(0.8f, {winSize.width / 2, winSize.height / 2});
        auto delay = DelayTime::create(1.2f);
        auto outMove = MoveTo::create(0.8f, {winSize.width + gameOverEffectNode->getContentSize().width, winSize.height / 2});
        gameOverEffectNode->runAction(Sequence::create(inMove,
                                                       delay,
                                                       outMove,
                                                       CallFunc::create([this]() {
                                                            auto resultScore = Value(_score);
                                                            LSGNavigator::Singleton::getInstance().dispatchCustomEvent("onGameFinish", &resultScore);
                                                        }),
                                                       nullptr));
        _uiViews->gameOverEffectNode = gameOverEffectNode;
    }
}

void GameModel::updateGameUI(float deltaTime)
{
    CC_UNUSED_PARAM(deltaTime);
    
    _uiViews->timerView->updateTime(_timer);
    
    _uiViews->scoreView->updateScore(_score);
    
    _uiViews->backGroundView->updateScroll();
}

void GameModel::updateLotterySpawnEnemy()
{
    std::random_device seed;
    std::mt19937 mersenneTwisterEngine(seed());
    std::uniform_int_distribution<int> lotteryDice(0, 300);
    const auto& winSize = Director::getInstance()->getWinSize();
    
    auto spawnPosition = Vec2::ZERO;
    switch (lotteryDice(mersenneTwisterEngine)) {
        case 0: {
            spawnPosition.x = winSize.width;
            std::uniform_real_distribution<double> randomY(winSize.height * 0.4f, winSize.height * 0.8f);
            spawnPosition.y = randomY(mersenneTwisterEngine);
            _enemyModel->addEnemy(EnemyType::Enemy1, spawnPosition);
            break;
        }
        case 10: {
            spawnPosition.x = winSize.width * 0.8f;
            _enemyModel->addEnemy(EnemyType::Enemy2, spawnPosition);
            break;
        }
        case 20: {
            spawnPosition = winSize;
            auto playerView = _playerModel->getUnitData()->getOwnerNode();
            auto bounce = EaseBounceInOut::create(MoveTo::create(5.f, playerView->getPosition()));
            auto moveTo = MoveTo::create(0.2f, {-1.f, winSize.height / 2});
            auto action = Sequence::create(bounce, moveTo, Hide::create(), nullptr);
            _enemyModel->addEnemy(EnemyType::Enemy3, spawnPosition, action);
            break;
        }
        default:
            break;
    }
}

void GameModel::updateMoveControllerByTouchBegan(Touch* touch)
{
    const auto& touchLocation = touch->getLocation();
    for (const auto& controller : _uiViews->controllerViews) {
        auto it = std::find_if(std::begin(_moveControllDatas), std::end(_moveControllDatas), [controller](decltype(_moveControllDatas)::value_type touchData) {
            return controller.first == touchData->controllerType;
        });
        if (it != std::end(_moveControllDatas) || controller.first == ControllerType::Shot)
            continue;
        if (controller.second->getBoundingBox().containsPoint(touchLocation)) {
            controller.second->setScale(1.2f);
            _moveControllDatas.push_back(std::make_shared<MoveControllData>(touch->getID(),
                                                                            controller.first,
                                                                            controller.second));
        }
    }
}

void GameModel::updateMoveControllerByTouchMoved(Touch* touch)
{
    const auto& touchLocation = touch->getLocation();
    for (const auto& controller : _uiViews->controllerViews) {
        if (controller.first == ControllerType::Shot)
            continue;
        if (controller.second->getBoundingBox().containsPoint(touchLocation)) {
            auto it = std::find_if(std::begin(_moveControllDatas), std::end(_moveControllDatas), [controller](decltype(_moveControllDatas)::value_type touchData) {
                return controller.first == touchData->controllerType;
            });
            if (it == std::end(_moveControllDatas)) {
                controller.second->setScale(1.2f);
                for (const auto& moveControll : _moveControllDatas) {
                    moveControll->controllerView->setScale(1.0f);
                }
                _moveControllDatas.clear();
                _moveControllDatas.push_back(std::make_shared<MoveControllData>(touch->getID(),
                                                                                controller.first,
                                                                                controller.second));
            }
        }
    }
}

void GameModel::removeMoveController(Touch* touch)
{
    auto it = std::remove_if(std::begin(_moveControllDatas), std::end(_moveControllDatas), [touch](decltype(_moveControllDatas)::value_type touchData) {
        return touch->getID() == touchData->id;
    });
    
    if (it != std::end(_moveControllDatas)) {
        (*it)->controllerView->setScale(1.0f);
        _moveControllDatas.erase(it, std::end(_moveControllDatas));
    }
}

void GameModel::updateShotControllerByTouch(Touch* touch)
{
    const auto& touchLocation = touch->getLocation();
    for (const auto& controller : _uiViews->controllerViews) {
        if (controller.first != ControllerType::Shot)
            continue;
        if (controller.second->getBoundingBox().containsPoint(touchLocation)) {
            _bulletModel->firingBullet(_particleBatchNode,
                                       _playerModel->getUnitData(),
                                       _playerModel->getUnitData()->getOwnerNode()->getPosition(),
                                       playerBulletAngle);
        }
    }
}

void GameModel::collisionForPlayerBulletVsEnemys(std::shared_ptr<Bullet> bullet)
{
    if (!bullet->getBulletData()->getIsAlive())
        return;
    for (const auto& enemy : _enemyModel->getEnemys()) {
        auto enemyView = enemy->getEnemyView();
        if (enemyView->getPosition().distance(bullet->getBulletView()->getPosition()) > bullet->getBulletView()->getContentSize().width) {
            continue;
        }
        if (enemyView->getBoundingBox().containsPoint(bullet->getBulletView()->getPosition())) {
            addParticleEffect(enemyView->getPosition());
            addScore(enemyCrushingScore);
            bullet->getBulletData()->setIsAlive(false);
            enemyView->setVisible(false);
        }
    }
}

Node* GameModel::addParticleEffect(const cocos2d::Vec2& spawnPosition)
{
    auto particle = LiziParticleView::create();
    particle->setPosition(spawnPosition);
    particle->runAction(Sequence::create(DelayTime::create(0.5f),
                                         ScaleTo::create(0.2f, 0.f),
                                         RemoveSelf::create(),
                                         nullptr));
    _particleBatchNode->addChild(particle);
    
    return particle;
}

void GameModel::addScore(int score)
{
    _score += score;
}

bool GameModel::isRejectTouch() const
{
    return _gameState != GameState::Exec;
}
