#pragma once

#include "LSGModel.hpp"
#include "PlayerModel.h"
#include "EnemyModel.h"
#include "BulletModel.h"
#include "Bullet.h"

class LSGController;
class GameBackGroundView;
class GameTimerView;
class GameScoreView;
class GameControllerView;
enum class ControllerType;
namespace ResourceFinder { enum class ParticleType0; }

class GameModel : public LSGModel
{
    enum class GameState {
        Enter,
        Exec,
        Exit
    };
    
    struct GameUIViews {
        GameUIViews()
        : rootNode(nullptr)
        , backGroundView(nullptr)
        , timerView(nullptr)
        , scoreView(nullptr)
        , getReadyEffectNode(nullptr)
        , gameOverEffectNode(nullptr)
        , controllerViews()
        {}
        
        ~GameUIViews() = default;
        
        cocos2d::Node* rootNode;
        GameBackGroundView* backGroundView;
        GameTimerView* timerView;
        GameScoreView* scoreView;
        cocos2d::Node* getReadyEffectNode;
        cocos2d::Node* gameOverEffectNode;
        std::map<ControllerType, GameControllerView*> controllerViews;
    };
    
    struct MoveControllData {
        explicit MoveControllData(int id, ControllerType controllerType, GameControllerView* controllerView)
        : id(id)
        , controllerType(controllerType)
        , controllerView(controllerView)
        {}
        
        ~MoveControllData() = default;
        
        int id;
        ControllerType controllerType;
        GameControllerView* controllerView;
    };
    
public:
    GameModel();
    virtual ~GameModel();
    
    bool init(LSGController* controller) override;
    void update(float deltaTime);
    
    void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
    void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
    void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
    void onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event);
    
private:
    void updateGameEnterState(float deltaTime);
    void updateGameExecState(float deltaTime);
    void updateGameExitState(float deltaTime);
    
    void updateGameUI(float deltaTime);
    void updateLotterySpawnEnemy();
    
    void updateMoveControllerByTouchBegan(cocos2d::Touch* touch);
    void updateMoveControllerByTouchMoved(cocos2d::Touch* touch);
    void removeMoveController(cocos2d::Touch* touch);
    void updateShotControllerByTouch(cocos2d::Touch* touch);
    void collisionForPlayerBulletVsEnemys(std::shared_ptr<Bullet> bullet);
    cocos2d::Node* addParticleEffect(const cocos2d::Vec2& spawnPosition);
    void addScore(int score);
    bool isRejectTouch() const;
    
    GameState _gameState;
    std::map<GameState, std::function<void(float)>> _gameUpdates;
    CC_SYNTHESIZE(float, _timer, Timer);
    CC_SYNTHESIZE(int, _score, Score);
    std::unique_ptr<GameUIViews> _uiViews;
    std::unique_ptr<PlayerModel> _playerModel;
    std::unique_ptr<EnemyModel> _enemyModel;
    std::unique_ptr<BulletModel> _bulletModel;
    std::vector<std::shared_ptr<MoveControllData>> _moveControllDatas;
    cocos2d::Node* _particleBatchNode;
};
