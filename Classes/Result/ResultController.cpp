#include "ResultController.h"
#include "ResourceFinder.h"
#include "LSGNavigator.h"
#include "TitleController.h"
#include "LSGLog.hpp"

USING_NS_CC;
using namespace ResourceFinder;

ResultController* ResultController::create(int resultScore)
{
    auto p = new ResultController();
    if (p && p->init(resultScore)) {
        p->autorelease();
    } else {
        CC_SAFE_DELETE(p);
    }
    return p;
}

bool ResultController::init(int resultScore)
{
    if (!LSGController::init())
        return false;
    auto highScore = UserDefault::getInstance()->getIntegerForKey("high_score", 0);
    if (resultScore > highScore) {
        UserDefault::getInstance()->setIntegerForKey("high_score", resultScore);
    }
    
    auto windowScore = Sprite::create(pathForImage(ImageType0::WindowScore));
    windowScore->setPosition(_winSize / 2);
    addChild(windowScore);
    return true;
}

bool ResultController::onTouchBegan(Touch* touch, Event* event)
{
    LSGNavigator::Singleton::getInstance().runScene(TitleController::create());
    return true;
}
