#pragma once

#include "LSGController.h"

class ResultController : public LSGController
{
public:
    static ResultController* create(int resultScore);
    
protected:
    ResultController() {}
    virtual ~ResultController() {}
    
    bool init(int resultScore);
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event) override;
};
