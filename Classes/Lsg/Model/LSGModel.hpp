#pragma once

#include "cocos2d.h"

class LSGController;

class LSGModel
{
public:
    virtual bool init(LSGController* controller = nullptr) {
        _controller = controller;
        return true;
    }
protected:
    CC_SYNTHESIZE(LSGController*, _controller, Controller);
};

template <class T>
class LSGSingleton
{
public:
    static T & getInstance() {
        if(!_uniqueInstance) {
            std::call_once(LSGSingleton<T>::_onceFlag,
                           [](){
                               LSGSingleton<T>::_uniqueInstance.reset(new T);
                           });
        }
        return *LSGSingleton<T>::_uniqueInstance;
    }
protected:
    LSGSingleton();
    virtual ~LSGSingleton();
private:
    LSGSingleton(LSGSingleton const&) = delete;
    LSGSingleton& operator=(LSGSingleton const&) = delete;
    static std::unique_ptr<T> _uniqueInstance;
    static std::once_flag _onceFlag;
};

template <class T> std::unique_ptr<T> LSGSingleton<T>::_uniqueInstance = nullptr;
template <class T> std::once_flag LSGSingleton<T>::_onceFlag;

#define LSG_DEFINE_AS_SINGLETON(CLASS) \
    friend class LSGSingleton<CLASS>; \
public: \
    virtual ~CLASS(); \
    using Singleton=LSGSingleton<CLASS>; \
private: \
    CLASS(); \
    CLASS(const CLASS& other) = delete; \
    const CLASS& operator=(const CLASS& other) = delete;
