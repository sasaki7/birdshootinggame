#include "LSGController.h"
#include "LSGNavigator.h"

USING_NS_CC;

LSGController::LSGController()
: _controllerTouchListener(nullptr)
, _winSize(Director::getInstance()->getWinSize())
{
}

LSGController::~LSGController()
{
}

bool LSGController::init()
{
    if (!Layer::init())
        return false;

    _controllerTouchListener = EventListenerTouchOneByOne::create();
    _controllerTouchListener->setSwallowTouches(true);

    _controllerTouchListener->onTouchBegan = std::bind(&LSGController::onTouchBegan, this, std::placeholders::_1, std::placeholders::_2);
    _controllerTouchListener->onTouchMoved = std::bind(&LSGController::onTouchMoved, this, std::placeholders::_1, std::placeholders::_2);
    _controllerTouchListener->onTouchCancelled = std::bind(&LSGController::onTouchCancelled, this, std::placeholders::_1, std::placeholders::_2);
    _controllerTouchListener->onTouchEnded = std::bind(&LSGController::onTouchEnded, this, std::placeholders::_1, std::placeholders::_2);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(_controllerTouchListener, this);
    return true;
}
