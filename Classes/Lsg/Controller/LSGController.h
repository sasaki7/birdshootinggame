#pragma once

#include "cocos2d.h"

class LSGController : public cocos2d::Layer
{
public:
    LSGController();
    virtual ~LSGController();
    
protected:
    // Virtual methods below can override
    virtual bool init();
    
    virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event) { return true; }
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {}
    virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event) {}
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {}
    
    cocos2d::EventListenerTouchOneByOne* _controllerTouchListener;
    cocos2d::Size _winSize;
};

#define CREATE_CONTROLLER_FUNC(__TYPE__) \
static __TYPE__* create() { \
    auto p = new __TYPE__(); \
    if (p && p->init()) { \
        p->autorelease(); \
    } else { \
        CC_SAFE_DELETE(p); \
    } \
    return p; \
}

