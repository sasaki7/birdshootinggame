#pragma once

#include "LSGModel.hpp"

class LSGController;

class LSGNavigator
{
    LSG_DEFINE_AS_SINGLETON(LSGNavigator);
public:
    static constexpr float defaultFadeDuration = 0.35f;
    
    void runScene(LSGController* controller, float fadeDuration = defaultFadeDuration);
    void addCustomEvent(const std::string& eventName, std::function<void(cocos2d::EventCustom* event)> callback);
    void dispatchCustomEvent(const std::string& eventName, void* optionalUserData = nullptr);
    void removeAllCustomEvent();
    
protected:
    cocos2d::Scene* createSceneForController(LSGController* controller);
    
private:
    cocos2d::Scene* _scene;
    CC_SYNTHESIZE_READONLY(LSGController*, _currentController, CurrentController);
    CC_SYNTHESIZE_READONLY(std::vector<std::string>, _customEventNames, CustomEventNames);
};
