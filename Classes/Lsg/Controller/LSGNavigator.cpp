#include "LSGNavigator.h"
#include "LSGController.h"
#include "Globals.h"

USING_NS_CC;

LSGNavigator::LSGNavigator()
: _scene(nullptr)
, _currentController(nullptr)
, _customEventNames()
{
}

LSGNavigator::~LSGNavigator()
{
}

void LSGNavigator::runScene(LSGController* controller, float fadeDuration)
{
    auto scene = createSceneForController(controller);
    auto director = Director::getInstance();
    auto transitionFade = TransitionFade::create(fadeDuration, scene);
    if (director->getRunningScene()) {
        director->replaceScene(transitionFade);
    } else {
        director->runWithScene(transitionFade);
    }
    CC_SAFE_RELEASE_NULL(_currentController);
    _currentController = controller;
    CC_SAFE_RETAIN(_currentController);
}

void LSGNavigator::addCustomEvent(const std::string& eventName, std::function<void(EventCustom* event)> callback)
{
    auto it = std::find(std::begin(_customEventNames), std::end(_customEventNames), eventName);
    if (it == std::end(_customEventNames)) {
        _customEventNames.push_back(eventName);
        Director::getInstance()->getEventDispatcher()->addCustomEventListener(eventName, callback);
    }
}

void LSGNavigator::dispatchCustomEvent(const std::string& eventName, void* optionalUserData)
{
    auto it = std::find(std::begin(_customEventNames), std::end(_customEventNames), eventName);
    if (it != std::end(_customEventNames)) {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(eventName, reinterpret_cast<void*>(optionalUserData));
    }
}

void LSGNavigator::removeAllCustomEvent()
{
    for (const auto& eventName : _customEventNames) {
        Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(eventName);
    }
    _customEventNames.clear();
    _customEventNames.shrink_to_fit();
}

Scene* LSGNavigator::createSceneForController(LSGController* controller)
{
    _scene = Scene::create();
    
//    auto centerAdjustmentOffset = (Director::getInstance()->getVisibleSize() -
//                                   Globals::Singleton::getInstance().getDesignResolution()) / 2;
//    controller->setPosition(controller->getPosition() + centerAdjustmentOffset);
    _scene->addChild(controller);

    return _scene;
}
