#pragma once

#ifdef COCOS2D_DEBUG
#if COCOS2D_DEBUG > 0
#include <cocos2d.h>

#define LSGLOG(arg, ...) \
cocos2d::log(\
    "LSGLOG: %s [Line:%d]:\n %s", \
    __PRETTY_FUNCTION__, \
    __LINE__, \
    cocos2d::StringUtils::format(arg, ##__VA_ARGS__).c_str()\
)
#else
    #define LSGLOG(arg, ...)
#endif
#else
    #define LSGLOG(arg, ...)
#endif
