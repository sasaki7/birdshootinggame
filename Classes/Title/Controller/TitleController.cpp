#include "TitleController.h"
#include "ResourceFinder.h"
#include "LSGNavigator.h"
#include "GameController.h"
#include "NativeBridge.h"

USING_NS_CC;
using namespace ResourceFinder;

bool TitleController::init()
{
    if (!LSGController::init())
        return false;
    auto titleHeaderImage = Sprite::create(pathForImage(ImageType0::TitleHeader));
    titleHeaderImage->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    titleHeaderImage->setPosition(_winSize.width / 2,
                                  _winSize.height / 2 + (titleHeaderImage->getContentSize().height * titleHeaderImage->getScale()));
    addChild(titleHeaderImage);

    auto startButton = MenuItemImage::create(pathForImage(ImageType0::PlayButton),
                                             pathForImage(ImageType0::PlayButton),
                                             [](Ref* sender) {
                                                 CC_UNUSED_PARAM(sender);
                                                 LSGNavigator::Singleton::getInstance().runScene(GameController::create());
                                             });
    auto buttonOffset = Vec2(120.f, 200.f);
    startButton->setPosition(_winSize.width / 2 - buttonOffset.x,
                             _winSize.height / 2 - buttonOffset.y);
    
    auto rankingButton = MenuItemImage::create(pathForImage(ImageType0::RankingButton),
                                               pathForImage(ImageType0::RankingButton),
                                               [](Ref* sender) {
                                                   CC_UNUSED_PARAM(sender);
                                                   NativeBridge::requestLoginGameCenter();
                                               });
    rankingButton->setPosition(_winSize.width / 2 + buttonOffset.x,
                               _winSize.height / 2 - buttonOffset.y);
    
    auto menu = Menu::create(startButton, rankingButton, nullptr);
    menu->setPosition(Vec2::ZERO);
    addChild(menu);
    return true;
}
