#pragma once

#include "LSGController.h"

class TitleController : public LSGController
{
public:
    CREATE_CONTROLLER_FUNC(TitleController);
    
protected:
    TitleController() = default;
    virtual ~TitleController() = default;
    
    bool init() override;
};
