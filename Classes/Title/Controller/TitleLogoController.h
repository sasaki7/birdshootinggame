#pragma once

#include "LSGController.h"

class TitleLogoController : public LSGController
{
public:
    CREATE_CONTROLLER_FUNC(TitleLogoController);
    
protected:
    TitleLogoController() {}
    virtual ~TitleLogoController() {}
    
    bool init() override;
};
