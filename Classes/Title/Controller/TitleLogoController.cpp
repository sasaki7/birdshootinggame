#include "TitleLogoController.h"
#include "TitleController.h"
#include "ResourceFinder.h"
#include "LSGNavigator.h"
#include "GameController.h"

USING_NS_CC;
using namespace ResourceFinder;

bool TitleLogoController::init()
{
    if (!LSGController::init())
        return false;
    auto logoSprite = Sprite::create(pathForImage(ImageType0::TitleLogo));
    logoSprite->setPosition(_winSize / 2);
    addChild(logoSprite);
    
    auto animationDuration = 1.5f;
    logoSprite->setOpacity(0);
    auto fadeIn = FadeIn::create(animationDuration);
    logoSprite->runAction(Sequence::create(fadeIn,
                                           DelayTime::create(animationDuration),
                                           fadeIn->reverse(),
                                           CallFunc::create([]() {
                                                LSGNavigator::Singleton::getInstance().runScene(TitleController::create());
                                           }), nullptr));
    return true;
}
