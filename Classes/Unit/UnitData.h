#pragma once

#include "cocos2d.h"
#include "Macros.h"

enum class UnitType {
    Unknown,
    Player,
    Enemy
};

enum class EnemyType {
    Enemy1,
    Enemy2,
    Enemy3,
    Enemy4,
    Boss
};

class UnitData
{
public:
    UnitData()
    : _life(0)
    , _unitType(UnitType::Unknown)
    , _ownerNode(nullptr)
    , _bulletShootingConditions(nullptr)
    {}
    
    virtual ~UnitData() = default;
    
protected:
    CC_SYNTHESIZE(int, _life, Life);
    CC_SYNTHESIZE(UnitType, _unitType, UnitType);
    CC_SYNTHESIZE(cocos2d::Node*, _ownerNode, OwnerNode);
    CC_SYNTHESIZE(std::function<bool(float)>, _bulletShootingConditions, BulletShootingConditions);
};

ENUM_HASH(UnitType)
ENUM_HASH(EnemyType)
