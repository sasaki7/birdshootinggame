#pragma once

#include "cocos2d.h"
#include "ResourceFinder.h"

class PlayerView : public cocos2d::Node
{
public:
    CREATE_FUNC(PlayerView);
    
    void updateView(ResourceFinder::PlayerImageType imageType);
protected:
    PlayerView() {}
    virtual ~PlayerView() {}
    
    bool init() override;
    
    std::map<ResourceFinder::PlayerImageType, cocos2d::Sprite*> _sprites;
};
