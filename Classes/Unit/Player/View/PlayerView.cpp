#include "PlayerView.h"

USING_NS_CC;
using namespace ResourceFinder;

bool PlayerView::init()
{
    if (!Node::init())
        return false;
    setCascadeOpacityEnabled(true);
    setCascadeColorEnabled(true);
    
    // @todo: スプライトアニメーション用のクラスを作りたい。
    const auto imageTypes = {
        PlayerImageType::Idle1,
        PlayerImageType::Idle2,
        PlayerImageType::Idle3,
        PlayerImageType::Idle4,
        PlayerImageType::Fly1,
        PlayerImageType::Fly2,
        PlayerImageType::Fly3,
        PlayerImageType::Hurt,
        PlayerImageType::Ko
    };
    Size totalSpriteSize;
    auto defaultShownImageType = PlayerImageType::Idle1;
    for (const auto& imageType : imageTypes) {
        auto sprite = Sprite::create(pathForImage(imageType));
        sprite->setVisible(imageType == defaultShownImageType);
        addChild(sprite);
        _sprites[imageType] = sprite;
        totalSpriteSize.width += sprite->getContentSize().width;
        totalSpriteSize.height += sprite->getContentSize().height;
    }
    
    setContentSize(totalSpriteSize / static_cast<float>(imageTypes.size()));
    return true;
}

void PlayerView::updateView(ResourceFinder::PlayerImageType imageType)
{
    std::for_each(std::begin(_sprites), std::end(_sprites), [imageType](decltype(_sprites)::const_reference element) {
        element.second->setVisible(element.first == imageType);
    });
}
