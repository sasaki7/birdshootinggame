#include "PlayerModel.h"
#include "PlayerView.h"
#include "LSGController.h"
#include "BulletModel.h"
#include "GameControllerView.h"

USING_NS_CC;
using namespace ResourceFinder;

namespace  {
    static const int defaultMaxPlayerLife = 3;
    static const float defaultBulletShootingInterval = 0.35f;
    static const int playerMoveDistanceOncePerFrame = 4.25f;
    static const float invisibleTime = 2.f;
    static const Vec2 invalidPosition = Vec2(-9999.f, -9999.f);
} // namespace

PlayerModel::PlayerModel()
: _unitData(nullptr)
, _targetMovePosition(invalidPosition)
, _playerState(PlayerState::Fly)
, _moveDistanceOncePerFrame(15.f)
, _animationIndex(0)
, _animationCounter(0.f)
, _animationInterval(0.25f)
, _bulletShootingCounter(0.f)
, _bulletShootingInterval(defaultBulletShootingInterval)
, _invisibleCounter(0.f)
{
}

PlayerModel::~PlayerModel()
{
}

bool PlayerModel::init(LSGController* controller)
{
    _unitData = std::make_shared<UnitData>();
    if (!LSGModel::init(controller) || !_unitData)
        return false;

    _unitData->setLife(defaultMaxPlayerLife);
    _unitData->setUnitType(UnitType::Player);
    _unitData->setOwnerNode(PlayerView::create());
    _unitData->getOwnerNode()->setPosition(Director::getInstance()->getWinSize() / 2);
    controller->addChild(_unitData->getOwnerNode());
    _unitData->setBulletShootingConditions([this](float deltaTime) {
        if (_playerState != PlayerState::Fly)
            return false;
        _bulletShootingCounter += deltaTime;
        if (_bulletShootingCounter >= _bulletShootingInterval) {
            _bulletShootingCounter = 0.f;
            return true;
        }
        return false;
    });
    return true;
}

void PlayerModel::update(float deltaTime)
{
//    updateMove();
    
//    updateBulletShooting(deltaTime);

    updateView(deltaTime);
    
//    updateState(deltaTime);
}

void PlayerModel::moveByController(ControllerType controllerType)
{
    auto moveDistance = Vec2::ZERO;
    switch (controllerType) {
        case ControllerType::Up:
            moveDistance.y += playerMoveDistanceOncePerFrame;
            break;
        case ControllerType::Down:
            moveDistance.y -= playerMoveDistanceOncePerFrame;
            break;
        case ControllerType::Left:
            moveDistance.x -= playerMoveDistanceOncePerFrame;
            break;
        case ControllerType::Right:
            moveDistance.x += playerMoveDistanceOncePerFrame;
            break;
        default:
            break;
    }
    _unitData->getOwnerNode()->setPosition(_unitData->getOwnerNode()->getPosition() + moveDistance);
}

void PlayerModel::clearTargetMovePosition()
{
    _targetMovePosition = invalidPosition;
}

bool PlayerModel::isRejectCollision() const
{
    return  _unitData->getLife() == 0 ||
            _invisibleCounter > 0.f ||
            _playerState == PlayerState::Hurt ||
            _playerState == PlayerState::Ko;
}

void PlayerModel::onDamage()
{
    _unitData->setLife(_unitData->getLife() - 1);
    _invisibleCounter = invisibleTime;
    _playerState = PlayerState::Hurt;
}

void PlayerModel::updateView(float deltaTime)
{
    static const std::map<PlayerState, PlayerImageType> stateToImageType = {
        {PlayerState::Idle, PlayerImageType::Idle1},
        {PlayerState::Fly, PlayerImageType::Fly1},
        {PlayerState::Hurt, PlayerImageType::Hurt},
        {PlayerState::Ko, PlayerImageType::Ko},
    };
    
    switch (getPlayerState()) {
        case PlayerState::Idle: {
        case PlayerState::Fly:
            static const std::map<PlayerState, int> stateToMaxAnimationInterval = {
                {PlayerState::Idle, 3},
                {PlayerState::Fly,  2},
            };
            setAnimationCouner(getAnimationCouner() + deltaTime);
            if (getAnimationCouner() > getAnimationInterval()) {
                setAnimationCouner(0.f);
                auto animationIndex = getAnimationIndex();
                animationIndex = (animationIndex + 1 > stateToMaxAnimationInterval.at(getPlayerState()))? 0 : animationIndex + 1;
                setAnimationIndex(animationIndex);
            }
            break;
        }
        default:
            _animationCounter = 0.f;
            _animationIndex = 0;
            break;
    }
    
    auto playerView = dynamic_cast<PlayerView*>(_unitData->getOwnerNode());
    if (playerView)
        playerView->updateView(static_cast<PlayerImageType>(static_cast<int>(stateToImageType.at(_playerState)) + _animationIndex));
}

void PlayerModel::updateState(float deltaTime)
{
    switch (getPlayerState()) {
        case PlayerState::Hurt: {
            _invisibleCounter -= deltaTime;
            if (_invisibleCounter < 0.f) {
                _invisibleCounter = 0.f;
                _playerState = PlayerState::Fly;
            }
            break;
        }
        default:
            break;
    }
}

void PlayerModel::updateMove()
{
    if (_playerState != PlayerState::Fly || _targetMovePosition.equals(invalidPosition))
        return;
    auto diff = _targetMovePosition - _unitData->getOwnerNode()->getPosition();
    if (diff.length() < _moveDistanceOncePerFrame) {
        _unitData->getOwnerNode()->setPosition(_targetMovePosition);
    } else {
        diff.normalize();
        auto movePosition = _unitData->getOwnerNode()->getPosition() + (diff * _moveDistanceOncePerFrame);
        _unitData->getOwnerNode()->setPosition(movePosition);
    }
}

void PlayerModel::updateBulletShooting(float deltaTime)
{
//    if (!_unitData->getBulletShootingConditions()(deltaTime))
//        return;
//    BulletModel::Singleton::getInstance().firingBullet(getController(),
//                                                       getUnitData(),
//                                                       getUnitData()->getOwnerNode()->getPosition(),
//                                                       playerBulletAngle);
}
