#pragma once

#include "LSGModel.hpp"
#include "ResourceFinder.h"
#include "Macros.h"
#include "UnitData.h"

enum class PlayerState
{
    Idle,
    Fly,
    Hurt,
    Ko
};

class PlayerView;
class LSGController;
enum class ControllerType;

class PlayerModel : public LSGModel
{
public:
    PlayerModel();
    virtual ~PlayerModel();
    
    bool init(LSGController* controller) override;
    void update(float deltaTime);

    void moveByController(ControllerType controllerType);
    LSG_DEPRECATED_ATTRIBUTE void clearTargetMovePosition();
    LSG_DEPRECATED_ATTRIBUTE bool isRejectCollision() const;
    LSG_DEPRECATED_ATTRIBUTE void onDamage();
    
protected:
    void updateView(float deltaTime);
    LSG_DEPRECATED_ATTRIBUTE void updateState(float deltaTime);
    LSG_DEPRECATED_ATTRIBUTE void updateMove();
    LSG_DEPRECATED_ATTRIBUTE void updateBulletShooting(float deltaTime);
    
private:
    CC_SYNTHESIZE(cocos2d::Vec2, _targetMovePosition, TargetMovePosition);
    CC_SYNTHESIZE(std::shared_ptr<UnitData>, _unitData, UnitData);
    CC_SYNTHESIZE(float, _bulletShootingCounter, BulletShootingCounter);
    CC_SYNTHESIZE(float, _bulletShootingInterval, BulletShootingInterval);
    CC_SYNTHESIZE(PlayerState, _playerState, PlayerState);
    CC_SYNTHESIZE(float, _moveDistanceOncePerFrame, MoveDistanceOncePerFrame);
    CC_SYNTHESIZE(int, _animationIndex, AnimationIndex);
    CC_SYNTHESIZE(float, _animationCounter, AnimationCouner);
    CC_SYNTHESIZE(float, _animationInterval, AnimationInterval);
    CC_SYNTHESIZE(float, _invisibleCounter, InvisibleCounter);
};
