#include <unordered_map>
#include "EnemyView.h"
#include "ResourceFinder.h"

USING_NS_CC;
using namespace ResourceFinder;

namespace {
    static const std::unordered_map<EnemyType, std::string> enemyTypeToImagePaths = {
        { EnemyType::Enemy1, pathForImage(EnemyImageType::Enemy1) },
        { EnemyType::Enemy2, pathForImage(EnemyImageType::Enemy2) },
        { EnemyType::Enemy3, pathForImage(EnemyImageType::Enemy3) },
        { EnemyType::Enemy4, pathForImage(EnemyImageType::Enemy4) },
        { EnemyType::Boss, pathForImage(EnemyImageType::Boss) }
    };
} // namespace

EnemyView* EnemyView::create(EnemyType enemyType)
{
    auto p = new EnemyView();
    if (p && p->init(enemyType)) {
        p->autorelease();
    } else {
        CC_SAFE_DELETE(p);
    }
    return p;
}

bool EnemyView::init(EnemyType enemyType)
{
    _enemyType = enemyType;
    return Sprite::initWithFile(enemyTypeToImagePaths.at(enemyType));
}
