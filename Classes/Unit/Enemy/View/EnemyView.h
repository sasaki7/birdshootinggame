#pragma once

#include "UnitData.h"

class EnemyView : public cocos2d::Sprite
{
public:
    static EnemyView* create(EnemyType enemyType);
    
protected:
    EnemyView() {}
    virtual ~EnemyView() {}
    
    bool init(EnemyType enemyType);
    
private:
    CC_SYNTHESIZE(EnemyType, _enemyType, EnemyType);
};
