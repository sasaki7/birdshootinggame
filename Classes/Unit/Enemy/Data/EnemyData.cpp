#include "EnemyData.h"

EnemyData::EnemyData(const Json::Value& rootJson)
: _unitData(std::make_shared<UnitData>())
, _enemyType()
{
    Json::Value::operator=(rootJson);
    if (rootJson != Json::Value::nullRef) {
        _unitData->setLife(rootJson.get("life", 0).asInt());
    }
}
