#pragma once

#include "UnitData.h"
#include "value.h"
#include "Macros.h"

class LSG_DEPRECATED_ATTRIBUTE_MSG("使用禁止(削除予定)") EnemyData : public Json::Value
{
public:
    EnemyData(const Json::Value& rootJson);
    virtual ~EnemyData() = default;
    
protected:
    std::shared_ptr<UnitData> _unitData;
    CC_SYNTHESIZE(EnemyType, _enemyType, EnemyType);
};
