#pragma once

#include "LSGModel.hpp"
#include "Enemy.h"
#include "value.h"

class LSGController;

class EnemyModel : public LSGModel
{
public:
    using Enemys = std::vector<std::shared_ptr<Enemy>>;
    
    EnemyModel();
    virtual ~EnemyModel();
    
    void update(float deltaTime);
    void addEnemy(EnemyType enemyType, const cocos2d::Vec2& spawnPoint, cocos2d::ActionInterval* moveAction = nullptr);
    
private:
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(Enemys, _enemys, Enemys);
};
