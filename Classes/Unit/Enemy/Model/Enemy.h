#pragma once

#include "EnemyData.h"

class EnemyView;

class Enemy
{
public:
    Enemy(EnemyType enemyType, EnemyView* enemyView);
    ~Enemy();

    void updateMove();
protected:
    CC_SYNTHESIZE(EnemyType, _enemyType, EnemyType);
    CC_SYNTHESIZE(EnemyView*, _enemyView, EnemyView);
};
