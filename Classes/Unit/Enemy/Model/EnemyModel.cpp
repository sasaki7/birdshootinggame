#include "EnemyModel.h"
#include "EnemyView.h"
#include "LSGController.h"

USING_NS_CC;

EnemyModel::EnemyModel()
: _enemys()
{
}

EnemyModel::~EnemyModel()
{
}

void EnemyModel::update(float deltaTime)
{
    for (auto it = std::begin(_enemys) ; it != std::end(_enemys) ;) {
        if ((*it)->getEnemyView()->isVisible()) {
            (*it)->updateMove();
            ++it;
        } else {
            it = _enemys.erase(it);
        }
    }
}

void EnemyModel::addEnemy(EnemyType enemyType, const cocos2d::Vec2& spawnPoint, ActionInterval* moveAction)
{
    auto enemyView = EnemyView::create(enemyType);
    enemyView->setPosition(spawnPoint);
    getController()->addChild(enemyView);
    
    if (moveAction) {
        enemyView->runAction(moveAction);
    }
    
    _enemys.push_back(std::make_shared<Enemy>(enemyType, enemyView));
}
