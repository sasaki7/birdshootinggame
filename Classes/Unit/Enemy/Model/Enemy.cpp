#include "Enemy.h"
#include "EnemyView.h"
#include "ResourceFinder.h"

USING_NS_CC;

Enemy::Enemy(EnemyType enemyType, EnemyView* enemyView)
: _enemyType(enemyType)
, _enemyView(enemyView)
{
}

Enemy::~Enemy()
{
    if (_enemyView)
        _enemyView->removeFromParent();
}

void Enemy::updateMove()
{
    switch (getEnemyType()) {
        case EnemyType::Enemy1: {
            auto moveTo = MoveTo::create(0.1f, {_enemyView->getPositionX() - 3.f, _enemyView->getPositionY()});
            _enemyView->runAction(moveTo);
            break;
        }
        case EnemyType::Enemy2: {
            float x = _enemyView->getPositionX();
            float y = _enemyView->getPositionY();
            ccBezierConfig bezier;
            bezier.controlPoint_1 = Director::getInstance()->getWinSize();
            bezier.controlPoint_2 = {0.f, static_cast<float>((Director::getInstance()->getWinSize().height * 0.8))};
            bezier.endPosition    = {x, y};
            auto bezierMove = BezierTo::create(20.f, bezier);
            auto action = Sequence::create(bezierMove, Hide::create(), nullptr);
            
            _enemyView->runAction(action);
            break;
        }
        default:
            break;
    }
}
